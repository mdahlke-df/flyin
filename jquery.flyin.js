/**
 * 
 * @param {function} $ jQuery
 * @param {object} window window object
 * @returns {undefined}
 */
(function ($, window) {

	$.fn.flyin = function (opts) {
		return this.each(function () {
			return new $fly($(this), opts);
		});
	};

	var ww = $(window).width();
	var wh = $(window).height();

	var defaults = {
		from: 'left',
		scrollOffset: 50, // wait this many extra pixels to flyin
		flyout: false, // make the elements flyout
		duration: 500, // animation duration
		delay: 0,
		easing: 'linear',
		afterFlyin: null,
		minWidth: 0,
		minHeight: 0
	};

	var instance = 0;

	$.flyin = function (el, opts) {

		this.options = $.extend(true, {}, defaults, opts);

		var that = this; // we can use "that" within $.each() functions
		this.el = el;
		this.items = this.el.find('.flyin-item');
		var scrollTop = $(window).scrollTop();
		var sph = scrollTop + wh; // scrollTop + windowHeight;
		var el; // current element in $.each();
		var elh; // current element height;
		var eltf; // element top offset;
		this.scrollOffset = that.options.scrollOffset;

		$.each(this.items, function (i, e) {
			instance++;
			that.items[i].landed = false;
			that.items[i].from = typeof $(e).attr('data-flyin-from') !== 'undefined' ? $(e).attr('data-flyin-from') : that.options.from;
			that.items[i].flyout = typeof $(e).attr('data-flyin-flyout') !== 'undefined' ? $(e).attr('data-flyin-flyout') : that.options.flyout;
			that.items[i].instance = typeof $(e).attr('data-flyin-instance') !== 'undefined' ? $(e).attr('data-flyin-instance') : instance;
			that.items[i].duration = typeof $(e).attr('data-flyin-duration') !== 'undefined' ? Number($(e).attr('data-flyin-duration')) : that.options.duration;
			that.items[i].delay = typeof $(e).attr('data-flyin-delay') !== 'undefined' ? Number($(e).attr('data-flyin-delay')) : that.options.delay;
			that.items[i].easing = typeof $(e).attr('data-flyin-easing') !== 'undefined' ? $(e).attr('data-flyin-easing') : that.options.easing;
			that.items[i].scrollOffset = typeof $(e).attr('data-flyin-scrollOffset') !== 'undefined' ? Number($(e).attr('data-flyin-scrollOffset')) : that.options.scrollOffset;
			that.items[i].offset = -(that.el.offset().left + that.el.width());
			that.items[i].minWidth = typeof $(e).attr('data-flyin-minWidth') === 'undefined' ? null : Number($(e).attr('data-flyin-minWidth'));
			that.items[i].minHeight = typeof $(e).attr('data-flyin-minHeight') === 'undefined' ? null : Number($(e).attr('data-flyin-minHeight'));

			if ((that.items[i].minWidth === null || ww > that.items[i].minWidth) && (that.items[i].minHeight === null || wh > that.items[i].minHeight)) {
				that.items[i].canFly = true;
			}
			else {
				that.items[i].canFly = false;
			}
			elh = $(e).height();
			eltf = $(e).offset().top + elh + that.items[i].scrollOffset;
			if (that.items[i].canFly && sph < eltf) {
				$(e).css(that.getCSS(that.items[i].from, that.items[i].offset));
			}
			that.items[i].left = that.items[i].from === 'left' ? -(that.el.offset().left + that.el.width()) : -(ww - that.el.offset().left);
		});

		$(window).scroll(function () {
			scrollTop = $(this).scrollTop();
			sph = scrollTop + wh; // scrollTop + windowHeight;
			$.each(that.items, function (i, e) {
				elh = $(e).height();
				eltf = $(e).offset().top + elh + that.items[i].scrollOffset;
				if (that.items[i].canFly && !that.items[i].landed && sph > eltf) {
					that.items[i].landed = true;
					setTimeout(function () {
						$(e).animate(that.getCSS(that.items[i].from, 0), {
							duration: that.items[i].duration,
							queue: false,
							easing: that.items[i].easing,
							complete: function () {
								if (that.options.afterFlyin) {
									that.options.afterFlyin(that);
								}
							}
						});
					}, that.items[i].delay);
				}
				else if (that.items[i].canFly && that.items[i].flyout && that.items[i].landed && sph < eltf) {
					that.items[i].landed = false;
					$(e).animate(that.getCSS(that.items[i].from, that.items[i].left), {
						duration: that.items[i].duration,
						queue: false
					});
					;
				}
			});
		});
		return this;
	};

	$fly = $.flyin;
	$fly.fn = $fly.prototype = {
		flyin: '0.0.4'
	};
	$fly.fn.extend = $fly.extend = $.extend;

	$fly.fn.extend({
		getCSS: function (from, amount) {
			var css;
			switch (from) {
				case 'right':
					css = {right: amount + 'px'};
					break;
				default:
					css = {left: amount + 'px'};
			}
			return css;
		}
	});

})(jQuery, window);